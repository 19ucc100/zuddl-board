import React from "react";
import Modal from "react-modal";

Modal.setAppElement("#root");

const DetailModal = ({ show, onClose, item }) => {
  return (
    <Modal
      isOpen={show}
      onRequestClose={onClose}
      className={"modal"}
      overlayClassName={"overlay"}
    >
      <div className={"close-btn-ctn"}>
        <h2 className="modal-title">{item.title}</h2>
        <button className="close-btn" onClick={onClose}>
          ❌
        </button>
      </div>
      <div>
        <h3>Description</h3>
        <p>{item.content}</p>
        <h3>Status</h3>
        <p>
          {item.icon}{" "}
          {`${item.status.charAt(0).toUpperCase()}${item.status.slice(1)}`}
        </p>
      </div>
    </Modal>
  );
};

export default DetailModal;
