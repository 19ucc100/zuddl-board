import React, { useState } from "react";
import Modal from "react-modal";
import { statuses } from "../data";

Modal.setAppElement("#root");

const AddModal = ({ show, onClose, setItems, items, status }) => {
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");

  const addItem = () => {
    if (!title) window.alert("Please enter title for task");
    if (!description) window.SubtleCrypto("Please enter description for task");

    const mapping = statuses.find((si) => si.status === status);

    const newItem = {
      id: items.length + 1,
      icon: mapping.icon,
      status: status,
      title: title,
      content: description,
    };
    console.log(newItem);
    setItems((prevState) => [newItem, ...prevState]);
    onClose();
  };

  return (
    <Modal
      isOpen={show}
      onRequestClose={onClose}
      className={"modal"}
      overlayClassName={"overlay"}
    >
      <div className={"close-btn-ctn"}>
        <h2 className="modal-title">Add new task</h2>
        <button className="close-btn" onClick={onClose}>
          ❌
        </button>
      </div>
      <form className="input-container" onSubmit={addItem}>
        <div>
          <h3>Title</h3>
          <input
            type="text"
            name="title"
            value={title}
            onChange={(e) => setTitle(e.target.value)}
          />
        </div>
        <div>
          <h3>Description</h3>
          <textarea
            value={description}
            onChange={(e) => setDescription(e.target.value)}
            rows={5}
          />
        </div>
        <button className="submitbtn" onClick={addItem}>
          Submit
        </button>
      </form>
    </Modal>
  );
};

export default AddModal;
