import React from "react";

const Header = () => {
  return (
    <div>
      <div className="row">
        <p className="page-header">Zuddl Board </p>
      </div>
      <div className="row addStatusRow">
        <button className="addStatusBtn">Add Status</button>
      </div>
    </div>
  );
};

export default Header;
