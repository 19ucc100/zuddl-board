import React, { useState, useRef } from "react";
import { useDrag, useDrop } from "react-dnd";
import DetailModal from "./DetailModal";
import ITEM_TYPE from "../data/types";

const Item = ({ item, index, moveItem, statusColor }) => {
  console.log(statusColor);
  const ref = useRef(null);

  const [, drop] = useDrop({
    accept: ITEM_TYPE,
    hover(item, monitor) {
      if (!ref.current) {
        return;
      }
      const dragIndex = item.index;
      const hoverIndex = index;

      if (dragIndex === hoverIndex) {
        return;
      }

      const hoveredRect = ref.current.getBoundingClientRect();
      const hoverMiddleY = (hoveredRect.bottom - hoveredRect.top) / 2;
      const mousePosition = monitor.getClientOffset();
      const hoverClientY = mousePosition.y - hoveredRect.top;

      if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
        return;
      }

      if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
        return;
      }
      moveItem(dragIndex, hoverIndex);
      item.index = hoverIndex;
    },
  });

  const [{ isDragging }, drag] = useDrag({
    type: ITEM_TYPE,
    item: { type: ITEM_TYPE, ...item, index },
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
    }),
  });

  const [showDetails, setShowDetails] = useState(false);

  const openDetailModal = () => setShowDetails(true);

  const closeDetailModal = () => setShowDetails(false);

  drag(drop(ref));

  return (
    <>
      <div
        ref={ref}
        style={{ opacity: isDragging ? 0 : 1 }}
        className={"item"}
        onClick={openDetailModal}
      >
        <div className="color-bar" style={{ backgroundColor: statusColor }} />
        <p className="item-title">{item.content}</p>
        <p className="item-status">{item.icon}</p>
      </div>
      <DetailModal item={item} onClose={closeDetailModal} show={showDetails} />
    </>
  );
};

export default Item;
