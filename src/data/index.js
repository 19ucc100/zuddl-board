const data = [
  {
    id: 1,
    icon: "🔆️",
    status: "to do",
    title: "Product Survey Form",
    content: "Get survey about our product using forms",
  },
  {
    id: 2,
    icon: "🔆️",
    status: "to do",
    title: "Purchase present",
    content: "Buy a birthday gift",
  },
  {
    id: 3,
    icon: "📝",
    status: "doing",
    title: "Check new tech",
    content: "Checkout new tech changes",
  },
  {
    id: 4,
    icon: "✅",
    status: "done",
    title: "Daily reading",
    content: "Finish reading Intro to React",
  },
];

const statuses = [
  {
    status: "to do",
    icon: "🔆️",
    color: "#843df5",
  },
  {
    status: "doing",
    icon: "📝",
    color: "#ec9c10",
  },
  {
    status: "done",
    icon: "✅",
    color: "#13e2b5",
  },
];

export { data, statuses };
