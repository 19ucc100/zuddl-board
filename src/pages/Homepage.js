import React, { useState } from "react";
import Item from "../components/Item";
import DropWrapper from "../components/DropWrapper";
import Col from "../components/Col";
import AddModal from "../components/AddModal";
import { data, statuses } from "../data";

const Homepage = () => {
  const [items, setItems] = useState(data);
  const [clickedStatus, setClickedStatus] = useState("to do");

  const [showAddModal, setShowAddModal] = useState(false);

  const openAddModal = (status) => {
    setClickedStatus(status);
    setShowAddModal(true);
  };

  const closeAddModal = () => setShowAddModal(false);

  const onDrop = (item, monitor, status) => {
    const mapping = statuses.find((si) => si.status === status);

    setItems((prevState) => {
      const newItems = prevState
        .filter((i) => i.id !== item.id)
        .concat({ ...item, status, icon: mapping.icon });
      return [...newItems];
    });
  };

  const moveItem = (dragIndex, hoverIndex) => {
    const item = items[dragIndex];
    setItems((prevState) => {
      const newItems = prevState.filter((i, idx) => idx !== dragIndex);
      newItems.splice(hoverIndex, 0, item);
      return [...newItems];
    });
  };

  return (
    <div className={"row"}>
      {statuses.map((s) => {
        return (
          <div key={s.status}>
            <div key={s.status} className={"col-wrapper"}>
              <h2 className={"col-header"}>{s.status.toUpperCase()}</h2>
              <DropWrapper onDrop={onDrop} status={s.status}>
                <Col>
                  {items
                    .filter((i) => i.status === s.status)
                    .map((i, idx) => (
                      <Item
                        key={i.id}
                        item={i}
                        index={idx}
                        moveItem={moveItem}
                        statusColor={s.color}
                      />
                    ))}
                </Col>
              </DropWrapper>
              <button onClick={() => openAddModal(s.status)}>
                Add new task
              </button>
            </div>
          </div>
        );
      })}
      {showAddModal && (
        <AddModal
          onClose={closeAddModal}
          show={showAddModal}
          setItems={setItems}
          items={items}
          status={clickedStatus}
        />
      )}
    </div>
  );
};

export default Homepage;
