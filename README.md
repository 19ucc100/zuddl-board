# Zuddl Board

- Its a web application to manage your tasks wisely.

### Preview :

- Live Link : https://zuddlboard.netlify.app/

![image](https://user-images.githubusercontent.com/56475750/211645186-8854bf29-e071-4d1f-9eb5-2a900b59d444.png)

![image](https://user-images.githubusercontent.com/56475750/211645621-8e379c27-75b6-40e0-befd-0072e3e03eec.png)

![image](https://user-images.githubusercontent.com/56475750/211645770-d5d95d89-f085-4d5d-b73c-1fe594b9a556.png)

Answers to assignment questions:

- In terms to database tables we can create two tables, one for tasks and another one for statuses / stages.
  Although, All the data can be covered in single table but I'm separating tasks and statuses for better individual management.

1. I'm representing stage as status of task and we can add as many statuses as per our need with any possible titles.

- To add new status, I've added a button `Add Status` which will open a model having an input field as `Status Label`
  and once we submit it, we can send a post request having all the data in body about new status like text, icon and color.

- The endpoint for this post request can be : `https://api.zuddlboard.com/status/add`

- To change the status we can have a edit icon / pencil icon after the status label, which can open similar modal onclick taking new label now.
  We can send a put request here having new label text in body.

- The endpoint for post this request can be : `https://api.zuddlboard.com/status/edit`

2. To have comments on task we can add a new property in our tasks table schema which can be an array of objects
   each having data about comment text, commenter, comment date and time etc.

- We are able to see all data about a task in a model onclick to card, in the same model the UI for comment feature can be added,
  Once we add a comment it will send a put request since we are updating a property of task ( i.e.comments ).

- The endpoint for post this request can be : `https://api.zuddlboard.com/task/{taskid}/edit`

3. For error handing we usually use try-catch block when we handle asynchronous api responses.
   Also, API itself can have some flags like isError and isSuccess that can be checked before we try getting data from the responce.
